function createNewUser() {
  const firstName = prompt("Введіть ім'я:");
  const lastName = prompt("Введіть прізвище:");
  const bbbb = prompt("Введіть дату народження:");

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: bbbb,
    getAge: function() {  //
      const currentDate = new Date();
      const currentYear = currentDate.getFullYear();
      const bDate = new Date(this.birthday);
      const birthYear = bDate.getFullYear();
      const age = currentYear - birthYear; 

      return age;
    },
    getPassword: function() {
      const firstLetter = this.firstName[0].toUpperCase();
      const lowLastName = this.lastName.toLowerCase();
      const bDate = new Date(this.birthday);
      const birthYear = bDate.getFullYear();
      const password = firstLetter + lowLastName + birthYear;
      
      return password;
    },
    getLogin: function() {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase() + this.getAge.toLowerCase();
    }
  };

  return newUser;
}

const user = createNewUser();
console.log(user.getPassword());





